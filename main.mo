import Debug "mo:base/Debug";

actor Dbank {
  stable var currentValue = 300;
  // currentValue := 100;

  let id = 102341848588184;
  let err = "Amount too large, currentValue less than zero."

  // Debug.print(debug_show(id));


 public func topUp (amount: Nat) {
  currentValue += amount;
  Debug.print(debug_show(currentValue));

};
 
 public func withwdrawl (amount: Nat) {
   let tempDate: Int = currentValue - amount;
   if(tempDate >= 0) {
    currentValue -= amount;
    Debug.print(debug_show(currentValue));
   } else {
     Debug.print(debug_show(err))
   };
  
};
 public query func checkBalance () anysc: Nat {
  return currentValue;  
};
// drawl()
//  topUp()
}

